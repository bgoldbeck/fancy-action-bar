local FAB = FancyActionBar
local LAM = LibAddonMenu2

function FAB.BuildMenu(SV, defaults)

	local panel = {
		type = 'panel',
		name = 'Fancy Action Bar',
		displayName = 'Fancy Action Bar',
		author = '|cFFFF00@andy.s|r',
		version = string.format('|c00FF00%s|r', FAB.GetVersion()),
		donation = 'https://www.esoui.com/downloads/info2311-HodorReflexes-DPSampUltimateShare.html#donate',
		registerForRefresh = true,
	}

	local options = {
		{
			type = "header",
			name = "|cFFFACDGeneral|r",
		},
		{
			type = "checkbox",
			name = "Show hotkeys",
			tooltip = "Show hotkeys under the action bar.",
			default = defaults.showHotkeys,
			getFunc = function() return SV.showHotkeys end,
			setFunc = function(value)
				SV.showHotkeys = value or false
				FAB.ToggleHotkeys()
			end,
		},
		{
			type = "checkbox",
			name = "Show highlight",
			tooltip = "Active skills will be highlighted.",
			default = defaults.showHighlight,
			getFunc = function() return SV.showHighlight end,
			setFunc = function(value)
				SV.showHighlight = value or false
			end,
		},
		{
			type = "checkbox",
			name = "Show arrow",
			tooltip = "Show an arrow near the current bar.",
			default = defaults.showArrow,
			getFunc = function() return SV.showArrow end,
			setFunc = function(value)
				SV.showArrow = value or false
				FAB_ActionBarArrow:SetHidden(not SV.showArrow)
			end,
		},
		{
			type = "colorpicker",
			name = "Arrow color",
			default = ZO_ColorDef:New(unpack(defaults.arrowColor)),
			getFunc = function() return unpack(SV.arrowColor) end,
			setFunc = function(r, g, b)
				SV.arrowColor = {r, g, b}
				FAB_ActionBarArrow:SetColor(unpack(SV.arrowColor))
			end,
		},
		{
			type = "checkbox",
			name = "Debug mode",
			tooltip = "Display internal events in the game chat.",
			default = false,
			getFunc = function() return FAB.IsDebugMode() end,
			setFunc = function(value)
				FAB.SetDebugMode(value or false)
			end,
		},
	}

	local name = FAB.GetName() .. 'Menu'
    LAM:RegisterAddonPanel(name, panel)
    LAM:RegisterOptionControls(name, options)

end